const URL_SERVER = 'https://raw.githubusercontent.com';

class Api {

    /**
     * [getMenu description]
     * @return {[type]} [description]
     */
    async getMenu() {

    	const query = await fetch(`${URL_SERVER}/Bernabe-Felix/Bellotero/master/app.json`);
        const data = await query.json();
        return data;
    }
}

export default new Api();

import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from '../Home';
import Testimonials from '../Testimonials';
import Configurator from '../Configurator';

const App = () => (
		<BrowserRouter>
			<Switch>
				<Route exact path="/" component={Home}></Route>
				<Route exact path="/page-1" component={Testimonials}></Route>
				<Route exact path="/page-2" component={Configurator}></Route>
			</Switch>
		</BrowserRouter>
	)

export default App;
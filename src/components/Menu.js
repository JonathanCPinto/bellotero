import React, { Component } from 'react';
import { connect } from 'react-redux';
import Link from './Link';
import API from '../Api'

class Menu extends Component {

    constructor(props){
      super(props);
      this.state = {
        menu: [],
      };
    }

    async componentDidMount(){
        const result = await API.getMenu()
        this.props.dispatch({
          type: 'SET_MENU',
          payload: result.menu.items,
        })
    }
     
    render() {
          let links = [];
          // data normal
          // let links = this.state.menu.map((item,i) => <Link key={i} item={item} />)
          // data redux
          if(this.props.myMenu[0])
             links = this.props.myMenu[0].map((item,i) => <Link key={i} item={item} />)
          return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark static-top Rectangle">
              <div className="container">
                <a className="navbar-brand" href="#">
                      <img src="img/bellotero.png" className="bellotero" />
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                      <span className="navbar-toggler-icon"></span>
                    </button>
                <div className="collapse navbar-collapse" id="navbarResponsive">
                  <ul className="navbar-nav ml-auto">
                    {links}
                  </ul>
                </div>
              </div>
            </nav>
            // <h1>Hola Mundo</h1>
          );
    }
}

const mapStateToProps = state => {
    return {
        myMenu: state.menu
    }
}

export default connect(mapStateToProps, null)(Menu);


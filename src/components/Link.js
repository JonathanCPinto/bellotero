import React, { Component } from 'react';

export default class Link extends Component {

    constructor(props){
      super(props);
    }
     
    render() {
          const {route, text} = this.props.item;
          return (
              <li className="nav-item active">
                      <a className="nav-link blue-primary" href={route}>{text}
                            <span className="sr-only">(current)</span>
                      </a>
              </li>
          );
    }
}


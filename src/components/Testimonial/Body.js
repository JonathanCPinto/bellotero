import React, { Component } from 'react';
import Carousel from '../Carousel/Carousel';

export default class Body extends Component {

    constructor(props){
      super(props);
      this.state = {
        title: '',
        reviews: []
      };
    }

    componentDidMount(){
        fetch('https://raw.githubusercontent.com/Bernabe-Felix/Bellotero/master/page1.json')
          .then(response => response.json())
          .then(responseJSON => {
            this.setState({
              title: responseJSON.slider.title,
              reviews: responseJSON.slider.reviews,
            })
          });
    }
     
    render() {
          return (
            <div className="container">
              <div className="row container-Our-customers-love-u">
                <div className="title-1">
                  <div className="Our-customers-love-u">
                    {this.state.title}  
                  </div>
                </div>
              </div>
              <Carousel items={this.state.reviews} />
            </div>
          );
    }
}


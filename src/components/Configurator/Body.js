import React, { Component } from 'react';
import Carousel from '../Carousel/Carousel';

export default class Body extends Component {

    constructor(props){
      super(props);
      this.state = {
        monthly_ingredient_spending: 36.211,
        time_employes: 8,
        estimated_food_cost: 0,
        estimated_annual_savings: 0,
        description: '',
        title: ''
      };
      this.handleChange = this.handleChange.bind(this);
    }

     handleChange(event) {
          const target = event.target;
          const value = target.type === 'checkbox' ? target.checked : target.value;
          const name = target.name;
          this.setState({
            [name]: value
          });
     }

    componentDidMount(){
        fetch('https://raw.githubusercontent.com/Bernabe-Felix/Bellotero/master/page2.json')
          .then(response => response.json())
          .then(responseJSON => {
            this.setState({
              title: responseJSON.calculator.title,
              description: responseJSON.calculator.description,
            })
          });
    }



    setResult = () => {
        //Estimated Food Cost Saving = Monthly ingredient spending * 0.3
        //Your estimated annual savings = Full-Time Employees * 1337 + Estimated Food Cost Saving
        this.state.estimated_food_cost = this.state.monthly_ingredient_spending*0.3;
        this.state.estimated_annual_savings = this.state.time_employes*1337 + this.state.estimated_food_cost;
    }

    setTitle = () => {
        let temp = this.state.title.split(' ');
        const title_1 = temp.pop();
        const title_2 = temp.join(' ');
        return {
          title_1,
          title_2
        }
    }
     
    render() {
          // console.log(this.state.title)
          this.setResult();
          let title = this.setTitle();
          return (
            <div className="container">
              <div className="row">
                <div className="col-sx-12 col-md-6 col-sm-6 col-lg-6">
                    <div className="row container-Our-customers-love-u">
                      <div className="title-1">
                        <div className="Our-customers-love-u">
                            {title.title_2}
                        </div>
                      </div>
                    </div>
                    <div className="row container-title-1">
                      <div className="title-1">
                        <div className="Our-customers-love-u">
                            {title.title_1}
                        </div>
                      </div>
                    </div>
                    <div className="row container-description-calculator">    
                        <p className="calculator-description">{this.state.description}</p>
                    </div>
                </div>
                <div className="col-sx-12 col-md-6 col-sm-6 col-lg-6">
                    <div className="container-calculator">
                        <div className="input-group mb-3">
                          <div className="input-group-prepend">
                            <span className="input-group-text" id="basic-addon1">$</span>
                          </div>
                          <input name="monthly_ingredient_spending" onChange={this.handleChange} value={this.state.monthly_ingredient_spending} type="text" className="form-control input-calculator" placeholder="" aria-label="Username" aria-describedby="basic-addon1" /> 
                        </div>  

                        <div className="input-group mb-3">
                          <input name="time_employes" onChange={this.handleChange} value={this.state.time_employes} type="text" className="form-control input-calculator" placeholder="" aria-label="Username" aria-describedby="basic-addon1" /> 
                        </div>  

                    </div>
                    <div className="container-results">
                        <div className="row">
                          <div className="col-sx-6 col-md-6 col-sm-6 col-lg-6">
                            <h1 className="result-calculator"> {'$'+this.state.estimated_food_cost.toFixed(2)}</h1>
                          </div>
                          <div className="col-sx-6 col-md-6 col-sm-6 col-lg-6">
                            <h1 className="result-calculator"> {'$'+this.state.estimated_annual_savings.toFixed(2)}</h1>
                          </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          );
      }
    }


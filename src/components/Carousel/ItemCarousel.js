import React, { Component } from 'react';

export default function ItemCarousel(props){
		const {comment, name, position} = props.item;
		const active = (props.active==0) ? 'active' : '';
		const class_carousel = `carousel-item ${active}`
		return (
			<div className={class_carousel}>
                <div className="row container-item-carousel">
                    <div className="col-sm-3 col-lg-3 col-md-12 col-xs-12 ">
                      <span className="name-carousel">{name}</span> <br />
                      <span className="position-carousel">{position}</span>
                  	</div>
                  <div className="col-sm-6 col-lg-6 col-md-12 col-xs-12 ">
                      <p className="comment-carousel">
                        {comment}
                      </p>
                  </div>
                </div>
            </div>
		)
}
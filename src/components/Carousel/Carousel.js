import React, { Component } from 'react';
import ItemCarousel from './ItemCarousel';

export default class Carousel extends Component {

    constructor(props){
      super(props);
      this.state = {
        title: '',
        reviews: []
      };
    }

 render() {

 		  let items_carousel = this.props.items.map((item,i) => <ItemCarousel key={i} item={item} active={i}/>)
          return (
          		<div className="row container-carousel">
                <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                    <ol className="carousel-indicators">
                      <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    </ol>
                    <div className="carousel-inner">
                      {items_carousel}
                    </div>
                    <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span className="sr-only">Previous</span>
                    </a>
                    <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <span className="carousel-control-next-icon" aria-hidden="true"></span>
                      <span className="sr-only">Next</span>
                    </a>
                  </div>
              </div>
          	);
    	}
    }


import React from 'react';
import logo from './logo.svg';
import './App.css';
import Menu from './components/Menu'
import Body from './components/Testimonial/Body'

function Testimonials() {
  return (
    <div className="App">
      <Menu />
      <Body />
    </div>
  );
}

export default Testimonials;
